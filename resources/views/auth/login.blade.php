@extends('layouts.app')

@section('content')
<section class="h-100 gradient-form">
  <div class="container py-5 h-100">
    <div class="row d-flex justify-content-center align-items-center h-100">
      <div class="col-xl-10">
        <div class="card rounded-3 text-black">
          <div class="row g-0">
            <div class="col-lg-6">
              <div class="card-body p-md-5 mx-md-4">

                <div class="text-center">
                  <img src="{{asset('Images/logo.png') }}"
                    style="width: 185px;" class="mb-5" alt="logo">
                </div>
                    <form method="POST" action="{{ route('login') }}">
                        @csrf

                        <div class="row mb-3">
                            <label for="email" class="col-md-4 col-form-label text-md-end">{{ __('Correo electronico') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="password" class="col-md-4 col-form-label text-md-end">{{ __('Contraseña') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-3">
                            <div class="col-md-6 offset-md-4">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label " for="remember">
                                        {{ __('Recordar') }}
                                    </label>
                                </div>
                            </div>
                        </div>

                        

                        <div class="row mb-0">
                            <div class="col-md-8 offset-md-4 d-grid gap-2 mx-auto">
                                <button type="submit" class="btn btn-outline-danger" >
                                    {{ __('Login') }}
                                </button>

                                @if (Route::has('password.request'))
                                    <a class="text-muted mx-auto" href="{{ route('password.request') }}">
                                        {{ ('¿Olvidaste tu contraseña?') }}
                                    </a>
                                @endif
                            </div>
                        </div>

                        <div class="d-flex align-items-center justify-content-center pb-4">
                    <p class="mb-0 me-2 mt-5">¿No tienes una cuenta?</p>
                    <a href="{{ route('register') }}" class="btn btn-outline-danger mt-5">Crear cuenta</a>
                  </div>
                  
                  </form>

</div>
</div>
<div class="col-lg-6 d-flex align-items-center gradient-custom-2">
<div class="text-white px-3 py-4 p-md-5 mx-md-4">
  <h4 class="mb-4">¡Bienvenido al mundo de los aceites esenciales! </h4>
  <p class="small mb-0">Inicia sesión para acceder a una amplia variedad de productos y recursos para mejorar tu salud y bienestar. Con tu cuenta podrás comprar aceites esenciales de alta calidad, acceder a tutoriales y recetas para elaborar tus propios productos y recibir actualizaciones y ofertas exclusivas. ¡No esperes más y comienza tu viaje hacia una vida más saludable y natural!</p>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</section>

@endsection

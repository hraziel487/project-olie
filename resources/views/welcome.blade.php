<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

        <!-- Styles -->
        <style>
            /*! normalize.css v8.0.1 | MIT License | github.com/necolas/normalize.css */html{line-height:1.15;-webkit-text-size-adjust:100%}body{margin:0}a{background-color:transparent}[hidden]{display:none}html{font-family:system-ui,-apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Helvetica Neue,Arial,Noto Sans,sans-serif,Apple Color Emoji,Segoe UI Emoji,Segoe UI Symbol,Noto Color Emoji;line-height:1.5}*,:after,:before{box-sizing:border-box;border:0 solid #e2e8f0}a{color:inherit;text-decoration:inherit}svg,video{display:block;vertical-align:middle}video{max-width:100%;height:auto}.bg-white{--tw-bg-opacity: 1;background-color:rgb(255 255 255 / var(--tw-bg-opacity))}.bg-gray-100{--tw-bg-opacity: 1;background-color:rgb(243 244 246 / var(--tw-bg-opacity))}.border-gray-200{--tw-border-opacity: 1;border-color:rgb(229 231 235 / var(--tw-border-opacity))}.border-t{border-top-width:1px}.flex{display:flex}.grid{display:grid}.hidden{display:none}.items-center{align-items:center}.justify-center{justify-content:center}.font-semibold{font-weight:600}.h-5{height:1.25rem}.h-8{height:2rem}.h-16{height:4rem}.text-sm{font-size:.875rem}.text-lg{font-size:1.125rem}.leading-7{line-height:1.75rem}.mx-auto{margin-left:auto;margin-right:auto}.ml-1{margin-left:.25rem}.mt-2{margin-top:.5rem}.mr-2{margin-right:.5rem}.ml-2{margin-left:.5rem}.mt-4{margin-top:1rem}.ml-4{margin-left:1rem}.mt-8{margin-top:2rem}.ml-12{margin-left:3rem}.-mt-px{margin-top:-1px}.max-w-6xl{max-width:72rem}.min-h-screen{min-height:100vh}.overflow-hidden{overflow:hidden}.p-6{padding:1.5rem}.py-4{padding-top:1rem;padding-bottom:1rem}.px-6{padding-left:1.5rem;padding-right:1.5rem}.pt-8{padding-top:2rem}.fixed{position:fixed}.relative{position:relative}.top-0{top:0}.right-0{right:0}.shadow{--tw-shadow: 0 1px 3px 0 rgb(0 0 0 / .1), 0 1px 2px -1px rgb(0 0 0 / .1);--tw-shadow-colored: 0 1px 3px 0 var(--tw-shadow-color), 0 1px 2px -1px var(--tw-shadow-color);box-shadow:var(--tw-ring-offset-shadow, 0 0 #0000),var(--tw-ring-shadow, 0 0 #0000),var(--tw-shadow)}.text-center{text-align:center}.text-gray-200{--tw-text-opacity: 1;color:rgb(229 231 235 / var(--tw-text-opacity))}.text-gray-300{--tw-text-opacity: 1;color:rgb(209 213 219 / var(--tw-text-opacity))}.text-gray-400{--tw-text-opacity: 1;color:rgb(156 163 175 / var(--tw-text-opacity))}.text-gray-500{--tw-text-opacity: 1;color:rgb(107 114 128 / var(--tw-text-opacity))}.text-gray-600{--tw-text-opacity: 1;color:rgb(75 85 99 / var(--tw-text-opacity))}.text-gray-700{--tw-text-opacity: 1;color:rgb(55 65 81 / var(--tw-text-opacity))}.text-gray-900{--tw-text-opacity: 1;color:rgb(17 24 39 / var(--tw-text-opacity))}.underline{text-decoration:underline}.antialiased{-webkit-font-smoothing:antialiased;-moz-osx-font-smoothing:grayscale}.w-5{width:1.25rem}.w-8{width:2rem}.w-auto{width:auto}.grid-cols-1{grid-template-columns:repeat(1,minmax(0,1fr))}@media (min-width:640px){.sm\:rounded-lg{border-radius:.5rem}.sm\:block{display:block}.sm\:items-center{align-items:center}.sm\:justify-start{justify-content:flex-start}.sm\:justify-between{justify-content:space-between}.sm\:h-20{height:5rem}.sm\:ml-0{margin-left:0}.sm\:px-6{padding-left:1.5rem;padding-right:1.5rem}.sm\:pt-0{padding-top:0}.sm\:text-left{text-align:left}.sm\:text-right{text-align:right}}@media (min-width:768px){.md\:border-t-0{border-top-width:0}.md\:border-l{border-left-width:1px}.md\:grid-cols-2{grid-template-columns:repeat(2,minmax(0,1fr))}}@media (min-width:1024px){.lg\:px-8{padding-left:2rem;padding-right:2rem}}@media (prefers-color-scheme:dark){.dark\:bg-gray-800{--tw-bg-opacity: 1;background-color:rgb(31 41 55 / var(--tw-bg-opacity))}.dark\:bg-gray-900{--tw-bg-opacity: 1;background-color:rgb(17 24 39 / var(--tw-bg-opacity))}.dark\:border-gray-700{--tw-border-opacity: 1;border-color:rgb(55 65 81 / var(--tw-border-opacity))}.dark\:text-white{--tw-text-opacity: 1;color:rgb(255 255 255 / var(--tw-text-opacity))}.dark\:text-gray-400{--tw-text-opacity: 1;color:rgb(156 163 175 / var(--tw-text-opacity))}.dark\:text-gray-500{--tw-text-opacity: 1;color:rgb(107 114 128 / var(--tw-text-opacity))}}
        </style>

        <style>
            body {
                font-family: 'Nunito', sans-serif;
            }
        </style>
    </head>
    <body class="antialiased">
    <nav class="navbar navbar-expand-md navbar-light pt-5 pb-4">
      <div class="container-xxl">
        <a href="#intro" class="navbar-brand">
          <span class="text-secondary fw-bold"
            ><i class="bi bi-book-half"></i> OLIÉ - Club de Clientes</span
          >
        </a>
        <button
          class="navbar-toggler"
          type="button"
          data-bs-toggle="collapse"
          data-bs-target="#main-nav"
          aria-controls="main-nav"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span class="navbar-toggler-icon"></span>
        </button>
        <div
          class="collapse navbar-collapse justify-content-end align-center"
          id="main-nav"
        >
          <ul class="navbar-nav">
          @if (Route::has('login'))
          @auth
            <li class="nav-item">
              <a href="{{ url('/home') }}" class="nav-link">Home</a>
            </li>
            @else
            <li class="nav-item">
              <a class="nav-link" href="{{ route('login') }}">Log in</a>
            </li>
            @if (Route::has('register'))
            <li class="nav-item">
              <a class="nav-link" href="{{ route('register') }}">Registro</a>
            </li>
            @endif
                    @endauth
                    @endif
          </ul>
        </div>
      </div>
    </nav>
   <!-- main image & intro text -->
   <section id="intro">
      <div class="container-lg">
        <div class="row g-4 justify-content-center align-items-center">
          <div class="col-md-5 text-center text-md-start">
            <h1>
              <div class="display-2">Difusor-Flora</div>
              <div class="display-5 text-muted">¡Mejora tu bienestar y bienestar emocional con Olie! Aceites esenciales 100% puros y naturales.</div>
            </h1>
            <p class="lead my-4 text-muted">
            ¿Estás buscando un aceite esencial de alta calidad para mejorar tu bienestar y bienestar emocional?
            </p>
            <a href="#pricing" class="btn btn-secondary btn-lg">Comprar Ahora</a>
            <a
              href="#sidebar"
              class="d-block mt-3"
              data-bs-toggle="offcanvas"
              role="button"
              aria-controls="sidebar"
              >¡Aprovecha nuestra oferta especial!</a
            >
          </div>
          <div class="col-md-5 text-center d-none d-md-block">
            <span
              class="tt"
              data-bs-placement="bottom"
              title="Net Ninja Book Cover"
            >
              <img
                src="https://www.olie.com.mx/wp-content/uploads/2022/06/Group-49-5.png"
                alt="ebook cover"
                class="img-fluid"
              />
            </span>
          </div>
        </div>
      </div>
    </section>


    <!-- topics at a glance -->
    <section id="topics">
      <div class="container-md">
        <div class="text-center mt-5">
          <h2>Dentro del Ebook...</h2>
          <p class="lead text-muted mt-5">
          ¡Descubre los beneficios de los aceites esenciales y cómo pueden mejorar tu bienestar y bienestar emocional con nuestro nuevo eBook gratuito "Descubriendo Olie"!
          </p>
        </div>
        <div class="row my-5 g-5 justify-content-around align-items-center">
          <div class="col-6 col-lg-4">
            <img
              src="https://www.olie.com.mx/wp-content/uploads/2022/08/Monochrome-Scrapbook-Reminder-Motivational-Quote-Instagram-Story.png"
              class="img-fluid"
              alt="ebook"
            />
          </div>
          <div class="col-lg-6">
            <div class="accordion" id="chapters">
              <div class="accordion-item">
                <h2 class="accordion-header" id="heading-1">
                  <button
                    class="accordion-button"
                    type="button"
                    data-bs-toggle="collapse"
                    data-bs-target="#chapter-1"
                    aria-expanded="true"
                    aria-controls="chapter-1"
                  >
                    Capitulo 1 - Aprenderás sobre los diferentes tipos de aceites esenciales
                  </button>
                </h2>
                <div
                  id="chapter-1"
                  class="accordion-collapse collapse show"
                  aria-labelledby="heading-1"
                  data-bs-parent="#chapters"
                >
                  <div class="accordion-body">
                    <p>
                    En este libro electrónico, aprenderás sobre los diferentes tipos de aceites esenciales y sus propiedades curativas únicas, así como también sobre cómo utilizarlos de manera segura y efectiva en tu vida diaria.
                    </p>
                    <p>
                    Descubrirás cómo el aceite esencial de eucalipto puede aliviar el dolor de cabeza, cómo el aceite esencial de lavanda puede ayudarte a relajarte antes de dormir, y cómo el aceite esencial de cítricos puede mejorar tu estado de ánimo.
                    </p>
                  </div>
                </div>
              </div>
              <div class="accordion-item">
                <h2 class="accordion-header" id="heading-2">
                  <button
                    class="accordion-button collapsed"
                    type="button"
                    data-bs-toggle="collapse"
                    data-bs-target="#chapter-2"
                    aria-expanded="false"
                    aria-controls="chapter-2"
                  >
                    Capitulo 2 - consejos
                  </button>
                </h2>
                <div
                  id="chapter-2"
                  class="accordion-collapse collapse"
                  aria-labelledby="heading-2"
                  data-bs-parent="#chapters"
                >
                  <div class="accordion-body">
                    <p>
                    Además, también te brindaremos consejos y trucos sobre cómo utilizar los aceites esenciales en difusores, masajes, baños, y cómo añadirlos a tus productos de limpieza y cuidado personal.
                    </p>
                    <p>
                    ¡Descarga nuestro eBook gratuito "Descubriendo Olie" ahora y comienza a disfrutar de los beneficios de los aceites esenciales hoy mismo!
                    </p>
                  </div>
                </div>
              </div>
              <div class="accordion-item">
                <h2 class="accordion-header" id="heading-3">
                  <button
                    class="accordion-button collapsed"
                    type="button"
                    data-bs-toggle="collapse"
                    data-bs-target="#chapter-3"
                    aria-expanded="false"
                    aria-controls="chapter-1"
                  >
                    Capitulo 3 - El poder de la aromaterapia
                  </button>
                </h2>
                <div
                  id="chapter-3"
                  class="accordion-collapse collapse"
                  aria-labelledby="heading-3"
                  data-bs-parent="#chapters"
                >
                  <div class="accordion-body">
                    <p>
                      Lorem, ipsum dolor sit amet consectetur adipisicing elit.
                      Quis assumenda delectus sapiente quidem consequatur odit
                      adipisci necessitatibus nemo aliquid minus modi tempore
                      quibusdam quas vitae, animi ipsam nulla sunt alias.
                    </p>
                    <p>
                      Lorem, ipsum dolor sit amet consectetur adipisicing elit.
                      Quis assumenda delectus sapiente quidem consequatur odit
                      adipisci necessitatibus nemo aliquid minus modi tempore
                      quibusdam quas vitae, animi ipsam nulla sunt alias.
                    </p>
                  </div>
                </div>
              </div>
             
        </div>
      </div>
    </section>

    <!-- reviews list -->
    <section id="reviews" class="bg-altlight">
      <div class="container-lg">
        <div class="text-center">
          <h2><i class="bi bi-stars text-altdark"></i> Reseñas del libro</h2>
          <p class="lead">Lo que los clientes han dicho sobre el libro......</p>
        </div>
        <div class="row justify-content-center my-5">
          <div class="col-lg-8">
            <div class="list-group">
              <div class="list-group-item py-3">
                <div class="pb-2">
                  <i class="bi bi-star-fill text-altdark"></i>
                  <i class="bi bi-star-fill text-altdark"></i>
                  <i class="bi bi-star-fill text-altdark"></i>
                  <i class="bi bi-star-fill text-altdark"></i>
                  <i class="bi bi-star-fill text-altdark"></i>
                </div>
                <h5 class="mb-1">Una libro obligado para todo aspirante a mejorar su bienestar</h5>
                <p class="mb-1">
                  Lorem ipsum dolor sit amet consectetur adipisicing elit.
                  Tenetur error veniam sit expedita est illo maiores neque quos
                  nesciunt, reprehenderit autem odio commodi labore praesentium
                  voluptate repellat in id quisquam.
                </p>
                <small>Review by Mario</small>
              </div>
              <div class="list-group-item py-3">
                <div class="pb-2">
                  <i class="bi bi-star-fill text-altdark"></i>
                  <i class="bi bi-star-fill text-altdark"></i>
                  <i class="bi bi-star-fill text-altdark"></i>
                  <i class="bi bi-star-fill text-altdark"></i>
                  <i class="bi bi-star-half text-altdark"></i>
                </div>
                <h5 class="mb-1">Lo mejor que he leido sobre el tema</h5>
                <p class="mb-1">
                  Lorem ipsum dolor sit amet consectetur adipisicing elit.
                  Tenetur error veniam sit expedita est illo maiores neque quos
                  nesciunt, reprehenderit autem odio commodi labore praesentium
                  voluptate repellat in id quisquam.
                </p>
                <small>Reseña de Luis</small>
              </div>
              
          </div>
        </div>
      </div>
    </section>

    <!-- contact form -->
    <section id="contact">
      <div class="container-lg">
        <div class="text-center">
          <h2>Ponte en contacto</h2>
          <p class="lead">
          ¿Preguntas? Rellene el formulario para ponerse en contacto conmigo directamente...
          </p>
        </div>
        <div class="row justify-content-center my-5">
          <div class="col-lg-6">
            <form>
              <label for="email" class="form-label">Email address:</label>
              <div class="input-group mb-4">
                <span class="input-group-text"
                  ><i class="bi bi-envelope-fill text-secondary"></i
                ></span>
                <input
                  type="email"
                  id="email"
                  class="form-control"
                  placeholder="e.g. mario@example.com"
                />
                <span class="input-group-text">
                  <span
                    class="tt"
                    data-bs-placement="bottom"
                    title="Enter an email address we can reply to."
                  >
                    <i class="bi bi-question-circle text-muted"></i>
                  </span>
                </span>
              </div>
              <label for="name" class="form-label">Name:</label>
              <div class="mb-4 input-group">
                <span class="input-group-text">
                  <i class="bi bi-person-fill text-secondary"></i>
                </span>
                <input
                  type="text"
                  id="name"
                  class="form-control"
                  placeholder="e.g. Mario"
                />
                <span class="input-group-text">
                  <span
                    class="tt"
                    data-bs-placement="bottom"
                    title="Pretty self explanatory really..."
                  >
                    <i class="bi bi-question-circle text-muted"></i>
                  </span>
                </span>
              </div>
              <label for="subject" class="form-label"
                >What is your question about?</label
              >
              <div class="mb-4 input-group">
                <span class="input-group-text">
                  <i class="bi bi-chat-right-dots-fill text-secondary"></i>
                </span>
                <select name="form-select" id="subject" class="form-select">
                  <option value="pricing" selected>Pricing query</option>
                  <option value="content">Content query</option>
                  <option value="other">Other query</option>
                </select>
              </div>
              <div class="mb-4 mt-5 form-floating">
                <textarea
                  id="query"
                  class="form-control"
                  style="height: 140px"
                  placeholder="query"
                ></textarea>
                <label for="query">Your query...</label>
              </div>
              <div class="mb-4 text-center">
                <button type="submit" class="btn btn-secondary">Submit</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </section>

         <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>

    </body>
</html>
